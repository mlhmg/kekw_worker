const express = require('express');
const router= express.Router();
const MQService = require("../services/MQService");
const { body, validationResult } = require('express-validator');

const sleep = ms => new Promise(r => setTimeout(r, ms));

// const logger = require('winston');
// require('winston-logstash');

// logger.add(logger.transports.Logstash,
//     {
//         port: 10329,
//         host: 'cc6ea62c-7b13-4cea-9a5d-86410e70dbc1-ls.logit.io',
//         ssl_enable: true,
//         max_connect_retries: -1,
//     });

const https = require('https');

const options = {
  hostname: 'api.logit.io',
//   port: 443,
  path: '/v2',
  method: 'POST',
  headers: {
    'Content-Type': 'application/json',
    'LogType': 'default',
    'ApiKey': 'cc6ea62c-7b13-4cea-9a5d-86410e70dbc1'
  },
};

const log_req = https.request(options, res => {
  console.log(`statusCode: ${res.statusCode}`);

  res.on('data', d => {
    process.stdout.write(d);
  });
});

log_req.on('error', error => {
  console.error(error);
});

const create_cake =  async (req, res, next) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(400).json({ errors: errors.array() });
    }

    email = req.body.email
    qty = req.body.qty
    cake_type = req.body.cake_type
    image_url = ""

    data_MQ = {
        "email" : email,
        "cake_type" : cake_type,
        "msg":"Pesanan diterima"
    }
    await MQService.publishToQueue(email, data_MQ);

    data_log = {}
    data_log['message'] = JSON.stringify(data_MQ)
    data_log['source'] = "Worker factory"
    data_log['function'] = "Membuat kue"
    log_req.write(JSON.stringify(data_log));
    log_req.end();

    await sleep(10000);

    data_MQ = {
        "email" : email,
        "cake_type" : cake_type,
        "msg":"Pesanan dibuat"
    }
    await MQService.publishToQueue(email, data_MQ);

    await sleep(10000);

    data_MQ = {
        "email" : email,
        "cake_type" : cake_type,
        "msg":"Pesanan dikirim"
    }
    await MQService.publishToQueue(email, data_MQ);

    await sleep(10000);

    data_MQ = {
        "email" : email,
        "cake_type" : cake_type,
        "msg":"Pesanan selesai"
    }
    await MQService.publishToQueue(email, data_MQ);

    switch(cake_type){
        case "strawberry":
            image_url = "/images/strawberry"
            break;
        case "chocolate":
            image_url = "/images/chocolate_cake"
            break;
        default:
            res.json({
                "error" : "invalid cake type"
            })
    }
    var payload = {
        "email": email,
        "qty": qty,
        "image_url": image_url
    }
    res.json(payload);
};

module.exports.create_cake= create_cake;