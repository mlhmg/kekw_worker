var express = require("express");
var app = express();
const path = require("path");

app.use(express.json()) 
app.use(express.urlencoded({ extended: true })) 

const cake_router = require("./routes/cake");
app.use("/cake", cake_router)

app.get('/images/:cake_name', function (req, res) {
    cake_name = req.params.cake_name
    res.sendFile(path.resolve(`./images/${cake_name}.png`));
})


app.listen(3000, () => {
 console.log("Server running on port 3000");
});