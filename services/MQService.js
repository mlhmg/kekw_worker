// import amqp from 'amqplib/callback_api';
const amqp =  require('amqplib/callback_api');
const CONN_URL = 'amqps://lmyzqody:LbHQ5ZkONsIUmf1Borxek0bps08i4KeS@armadillo.rmq.cloudamqp.com/lmyzqody';

let ch = null;
amqp.connect(CONN_URL, function (err, conn) {
   conn.createChannel(function (err, channel) {
      ch = channel;
   });
});

const publishToQueue = async (queueName, data) => {
    ch.assertQueue(queueName, {
        durable: false,
        // exclusive: true 
      });
    ch.sendToQueue(queueName, Buffer.from(JSON.stringify(data)));
    console.log(" [x] Sent %s", data);
 }

 process.on('exit', (code) => {
    ch.close();
    console.log(`Closing rabbitmq channel`);
 });
 module.exports.publishToQueue= publishToQueue;