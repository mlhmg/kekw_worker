const express = require("express");

const cake_controller = require("../controllers/cake"); 

const router = express.Router();
const { body, validationResult } = require('express-validator');


router.post('/', body('email').isEmail(), cake_controller.create_cake);

module.exports=router;